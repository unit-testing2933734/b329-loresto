const { names, users } = require('../src/util.js');

module.exports = (app) => {
    app.get('/', (req, res) => {
        return res.send({'data': {} });
    });

    app.get('/people', (req, res) => {
        return res.send({
            people: names
        });
    });

    app.post('/person', (req, res) => {
        if(!req.body.hasOwnProperty('name')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter NAME'
            });
        };
        if(typeof req.body.name !== 'string'){
            return res.status(400).send({
                'error': 'Bad Request - NAME has to be a string'
            });
        };
        if(!req.body.hasOwnProperty('age')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter AGE'
            });
        };
        if(typeof req.body.age !== "number"){
            return res.status(400).send({
                'error': 'Bad Request - AGE has to be a number'
            });
        };
        if(!req.body.hasOwnProperty('username')){
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter USERNAME'
            });
        };
    });

    // S4 Activity Template START
    app.post('/login',(req,res)=>{

        let foundUser = users.find((user) => {

            return user.username === req.body.username && user.password === req.body.password

        });

        if(!req.body.hasOwnProperty('username')){
            // Add code here to pass the test
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter username'
            });
        }

        if(!req.body.hasOwnProperty('password')){
            // Add code here to pass the test
            return res.status(400).send({
                'error': 'Bad Request - missing required parameter password'
            });
        }

        if(foundUser){
            // Add code here to pass the test
            return res.send({'message' : 'Login successful'});
        }

        let userCheck = users.find((user) => {

            if(user.username === req.body.username){
                let isPasswordCorrect = user.password === req.body.password

                if(!isPasswordCorrect){
                    return res.status(403).send({
                        'error': 'Invalid credentials'
                    });
                };
            } else {
                return res.status(404).send({
                    'error': 'User not found'
                });
            };

        });

        // Stretch goal
        // if(!foundUser){
            // Add code here to pass the test
        // }

        // foundUser already invalidates if the user is not existing.
        // For the stretch goals... I created a new checker if the user is existing before checking if the password is correct.
    })
    // S4 Activity Template END

};