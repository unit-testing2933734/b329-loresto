function factorial(n){
    if(typeof n !== 'number') return undefined; // will use in s04
    // GREEN - Write code to pass the test
    if(n<0) return undefined; // will use in s04
    if(n===0) return 1;
    if(n===1) return 1;
    return n * factorial(n-1);
    // 4 * 3 * 2 * 1
}

module.exports = {
    factorial: factorial
}

// start of activity
function div_check(n){
    return (n % 5 === 0) || (n % 7 === 0);
}

const names = {
    "Aki" : {
        "alias": "Aki",
        "name": "Akihisa Yoshii",
        "age": 17
    },
    "Mizuki" : {
        "alias": "Mizuki",
        "name": "Mizuki Himeji",
        "age": 16
    },
}

// S4 Activity Template START
const users = [
    {
        username: "brBoyd87",
        password: "87brandon19"

    },
    {
        username: "tylerofsteve",
        password: "stevenstyle75"
    }
]
// S4 Activity Template END


module.exports = {
    factorial: factorial,
    div_check: div_check,
    names: names,
    users: users
}