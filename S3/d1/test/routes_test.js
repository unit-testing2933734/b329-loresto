const chai = require('chai');

const expect = chai.expect;
// same as const {expect} = require('chai');

const http = require('chai-http');
chai.use(http);

describe('api_test_suite', () => {

	it('test_api_people_is_running', () => {
		chai.request('http://localhost:5001')
		// '.get()' specifies the type of http request as GET and accepts the API endpoint
		.get('/people')
		// '.end()' is the method that handles the response and error that will be received from the endpoint
		.end((err, res) => {
			// '.not' negates all assertions that follow in the chain
			expect(res).to.not.equal(undefined);
		});
	});

	it('test_api_people_returns_200', (done) => {
		chai.request('http://localhost:5001')
		.get('/people')
		.end((err, res) => {
			expect(res.status).to.equal(200);
			// '.done()' function is typically used in asynchronous test cases to signal that the case is complete
			done();
		});
	});

	it('test_api_post_person_returns_400_if_no_person_name', (done) => {
		chai.request('http://localhost:5001')
		// '.post()' specifies the type of http request as POST and accepts the API endpoint
		.post('/person')
		// './type()' specifies the type of input to be sent out as part of the POST request
		.type('json')
		// './send()' specifies the data to be sent as part of POST request
		.send({
			alias: "Minami",
			age: 17
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		});
	});

});

// ACTIVITY

describe('api_test_suite_users', () => {

	it('test_api_post_person_is_running', () => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			alias: "Minami",
	        name: "Minami Shimada",
	        age: 17
		})
		.end((err, res) => {
			expect(res).to.not.equal(undefined);
		});
	});

	it('test_api_post_person_is_returns_400_if_no_username', (done) => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			alias: "Minami",
	        name: "Minami Shimada",
	        age: 17
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		});
	});

	it('test_api_post_person_is_returns_400_if_no_age', (done) => {
		chai.request('http://localhost:5001')
		.post('/person')
		.type('json')
		.send({
			username: "Minami",
	        name: "Minami Shimada"
		})
		.end((err, res) => {
			expect(res.status).to.equal(400);
			done();
		});
	});

});