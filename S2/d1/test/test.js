const { factorial, div_check } = require('../src/util.js');

// gets the expect and assert functions from chai to be used
const {expect, assert} = require('chai');

// Test Suites are made up of collection of test cases that should be executed together

// "describe()" keyword is used to group tests together
describe('test_fun_factorials', () => {

	// "it()" is used to define a single test case
	// "it()" accepts two parameters
	// a string explaining what the test should do
	// callback function which contains the actual test
	it('test_fun_factorial_5!_is_120', () => {
		const product = factorial(5);
		expect(product).to.equal(120);
	})

	it('test_fun_factorial_1!_is_1', () => {
		const product = factorial(1);
		assert.equal(product, 1)
	})

	// start of activity
	it('test_fun_factorial_0!_is_1', () => {
		const product = factorial(0);
		expect(product).to.equal(1);
	})

	it('test_fun_factorial_4!_is_24', () => {
		const product = factorial(4);
		expect(product).to.equal(24);
	})

	it('test_fun_factorial_10!_is_3628800', () => {
		const product = factorial(10);
		assert.equal(product, 3628800);
	})
	
})

describe('test_divisibility_by_5_or_7', () => {
	it('test_100_is_divisible_by_5_or_7', () => {
		const isDivisible = div_check(100);
		expect(isDivisible).to.be.true;
	})

	it('test_49_is_divisible_by_5_or_7', () => {
		const isDivisible = div_check(49);
		expect(isDivisible).to.be.true;
	})

	it('test_30_is_divisible_by_5_or_7', () => {
		const isDivisible = div_check(30);
		expect(isDivisible).to.be.true;
	})

	it('test_56_is_divisible_by_5_or_7', () => {
		const isDivisible = div_check(56);
		expect(isDivisible).to.be.true;
	})
})

